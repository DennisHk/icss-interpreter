package nl.han.ica.icss.checker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import nl.han.ica.icss.ast.*;
import nl.han.ica.icss.ast.literals.ColorLiteral;
import nl.han.ica.icss.ast.literals.PercentageLiteral;
import nl.han.ica.icss.ast.literals.PixelLiteral;
import nl.han.ica.icss.ast.literals.ScalarLiteral;
import nl.han.ica.icss.ast.operations.MultiplyOperation;
import nl.han.ica.icss.ast.types.*;
import nl.han.ica.icss.parser.ICSSParser;

public class Checker {
	public void check(AST ast) {
		walkTreeStart(ast);
		HashMap p = new HashMap<String, String>();
		System.out.println(p.isEmpty());
		test(p);
		System.out.println(p.isEmpty());
		System.out.println(ast);
	}

	private void test(HashMap p) {
		p.put("appel", "banaan");
		System.out.println(p.isEmpty());
	}

	private void walkTreeStart(AST ast) {
		walkTree(ast.root, new HashMap<>());
	}

	//To keep the funcitonality of local variables intact, I've changed the linkedlist of hashmaps to a local hashmap.
	//Every step down the tree this hashmap is passed along and whenever a variable is changed or added, it's simply
	//added to this hashmap. This way every node will have a hashmap with every variable instantiated to that point,
	//and changing the value of a variable won't affect other parts of the program.
	private void walkTree(ASTNode node, HashMap<String, ExpressionType> variables) {
		ArrayList<ASTNode> children = node.getChildren();
		checkNode(node, variables);
		HashMap<String, ExpressionType> newVariables = variables;
		if (node instanceof Stylesheet || node instanceof Stylerule) {
			newVariables = updateVariables(node, variables);
		}
		if (children.size() > 0) {
			for (ASTNode nextNode : children) {
			    walkTree(nextNode, newVariables);
			}
		}
	}

	//This function will check if a node is a type that needs checking, if so, the corresponding checking function 
	// will be called.
	private void checkNode(ASTNode node, HashMap<String, ExpressionType> variables) {
		if (node instanceof Declaration) {
			checkDeclaration((Declaration) node, variables);
		}
	}

	private void checkDeclaration(Declaration node, HashMap<String, ExpressionType> variables) {
		ExpressionType expressionType = getExpressionTypeOfNode(node.expression, variables);
		if (Arrays.asList("background-color", "color").contains(node.property.name)) {
			if (expressionType != ExpressionType.COLOR) {
				node.setError(node.property.name + " must be followed with a color (ex. #AF23C4). The current value cannot be computed.");
			}
		} else  {
			if (expressionType != ExpressionType.PIXEL && expressionType != ExpressionType.PERCENTAGE) {
				node.setError(node.property.name + " must be followed with a pixel value (ex. 120px or 33%). The current value cannot be computed.");
			}
		}
	}

	private HashMap<String, ExpressionType> updateVariables(ASTNode node, HashMap<String, ExpressionType> variables) { //look for any variable declaratiosn and update the hashmap if any are found
		boolean change = false;
		HashMap<String, ExpressionType> newVariables = clone(variables);
		for (ASTNode currentNode : node.getChildren()) {
			if (currentNode instanceof VariableAssignment) {
				String variableName = ((VariableAssignment) currentNode).name.name;
				ExpressionType expressionType = getExpressionTypeOfNode(((VariableAssignment) currentNode).expression, variables);
				newVariables.put(variableName, expressionType);
				change = true;
			}
		}
		if (change) {
			return newVariables;
		} else {
			return variables;
		}
	}


	//returns the expression type of a node (PIXEL, PERCENTAGE, COLOR or SCALAR. When the node is a calculation
    //we need to use different ways to determine the type. When the node is a variable reference, we look in the
    //hashmap to get the type.
	private ExpressionType getExpressionTypeOfNode(Expression expression, HashMap<String, ExpressionType> variables) {
		if (expression instanceof PixelLiteral) {
			return ExpressionType.PIXEL;
		} else if (expression instanceof PercentageLiteral) {
			return ExpressionType.PERCENTAGE;
		} else if (expression instanceof ColorLiteral) {
			return ExpressionType.COLOR;
		} else if (expression instanceof ScalarLiteral) {
			return ExpressionType.SCALAR;
		} else if (expression instanceof Operation) {
			return getExpressionTypeOfCalculation((Operation) expression, variables);
		} else if (expression instanceof VariableReference) {
			return getExpressionTypeOfVariableReference((VariableReference) expression, variables);
		} else {
			throw new IllegalArgumentException("ExpressionType got an expression it couldn't handle, please help him parse " + expression.getClass().getName() + "without failing.");
		}
	}

	//Fetch the expression type of a variable, or throw an error if it does not exist.
	private ExpressionType getExpressionTypeOfVariableReference(VariableReference expression, HashMap<String, ExpressionType> variables) {
		if (variables.containsKey(expression.name)) {
			return variables.get(expression.name);
		} else {
			expression.setError("The variable " + expression.name + " was never assigned a value.");
			return ExpressionType.UNDEFINED;
		}
	}

	//Calculations have two sides. The result of an add/subtract calculation is either one of the side's types, since
    //additions are only legal when both sides are the same. In a calculation the resulting value is the side which is
    //not a scalar. ex. 2*4px would return 8px, a pixel type. This function works recusively together with the
    //getExpressionTypeOfNode() function to check long calculations.
	private ExpressionType getExpressionTypeOfCalculation(Operation operation, HashMap<String, ExpressionType> variables) {
		checkOperation(operation, variables);
		ExpressionType expressionTypel = getExpressionTypeOfNode(operation.lhs, variables);
		ExpressionType expressionTyper = getExpressionTypeOfNode(operation.rhs, variables);
		if (expressionTypel == ExpressionType.SCALAR) { //When the left one is a scalar, we want to return the other value (if they're both scalars, scalar will still be returned). This is to make sure multiplications return the right value.
			return expressionTyper;
		} else { //at this point either the right one is a scalar, or they're both no scalar. Eitherway, returning the left value will give the desired result.
			return expressionTypel;
		}
	}

	//Check that the calculation is actually valid, 4+5px would be impossible, multiplying without scalars is impossible
    //and any calculation with colors is impossible.
	private void checkOperation(Operation operation, HashMap<String, ExpressionType> variables) {
		ExpressionType expressionTypel = getExpressionTypeOfNode(operation.lhs, variables);
		ExpressionType expressionTyper = getExpressionTypeOfNode(operation.rhs, variables);

		if (expressionTypel == ExpressionType.COLOR || expressionTyper == ExpressionType.COLOR) { //No calculations can be done with colors.
			operation.setError("Can't calculate with colors");
		}

		if (operation instanceof MultiplyOperation) {
			if (expressionTypel != ExpressionType.SCALAR && expressionTyper != ExpressionType.SCALAR) { // You can't multiply two of the same values, except if one of the two is a scalar.
				operation.setError("Can't multiply these two values");
			}
		} else {
			if (expressionTypel != expressionTyper) {
				operation.setError("Can't add/subtract two different types from each other");
			}
		}
	}

	//Since hashmaps are pseudo-global (they just reference a memory location, so editing in one place will edit 
	// everywhere), I need to clone the hashmap every time changes are made. This will be done here.
	private HashMap<String, ExpressionType> clone(HashMap<String, ExpressionType> original) {
		return new HashMap<String, ExpressionType>(original);
	}
}
