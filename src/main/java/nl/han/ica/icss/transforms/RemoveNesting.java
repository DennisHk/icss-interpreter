package nl.han.ica.icss.transforms;

import nl.han.ica.icss.ast.AST;
import nl.han.ica.icss.ast.ASTNode;
import nl.han.ica.icss.ast.Stylerule;
import nl.han.ica.icss.ast.Stylesheet;

import java.util.ArrayList;

public class RemoveNesting implements Transform {
	@Override
	public void apply(AST ast) {
		removeNesting(ast.root);
	}

	//starts the nesting removal. looks through the root and calls the other removeNesting funcion on the node it it's
	//a stylerule. Only stylerules can contain other stylerules so those are the only nodes we check. After this it
	//adds all nodes in the addToRoot list to the root.
	private void removeNesting(Stylesheet root) {
		ArrayList<Stylerule> addToRoot = new ArrayList<>();
		for (ASTNode currentNode : root.getChildren()) {
			if (currentNode instanceof Stylerule) {
				removeNesting((Stylerule) currentNode, addToRoot);
			}
		}

		for (Stylerule node : addToRoot) {
			root.addChild(node);
		}
	}

	//walk through all children of the node. If they're a stylerule, remove them from this node and add them to the addToRoot list.
	private void removeNesting(Stylerule node, ArrayList<Stylerule> addToRoot) {
		for (ASTNode currentNode : node.getChildren()) {
			if (currentNode instanceof Stylerule) {
				((Stylerule) currentNode).selectors.addAll(node.selectors); //Stiekem niet wat de originele schrijver had gewild, eigenlijk moet ik alle kinderen van currentnode opcragen en hier alle selectoren van zoeken en deze toevoegen aan de node. Ik vind dit mooier.
				node.removeChild(currentNode);
				addToRoot.add((Stylerule) currentNode);
				removeNesting((Stylerule) currentNode, addToRoot);
			}
		}
	}
}
