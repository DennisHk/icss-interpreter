package nl.han.ica.icss.transforms;

import nl.han.ica.icss.ast.*;
import nl.han.ica.icss.ast.literals.PercentageLiteral;
import nl.han.ica.icss.ast.literals.PixelLiteral;
import nl.han.ica.icss.ast.literals.ScalarLiteral;
import nl.han.ica.icss.ast.operations.AddOperation;
import nl.han.ica.icss.ast.operations.MultiplyOperation;
import nl.han.ica.icss.ast.operations.SubtractOperation;

import java.util.ArrayList;
import java.util.HashMap;

public class EvalExpressions implements Transform {


	public EvalExpressions() {

	}

	@Override
	public void apply(AST ast) {
		walkTree(ast);
	}

	private void walkTree(AST ast) {
		walkTree(ast.root, new HashMap<>());
	}

	//To keep the funcitonality of local variables intact, I've changed the linkedlist of hashmaps to a local hashmap.
	//Every step down the tree this hashmap is passed along and whenever a variable is changed or added, it's simply
	//added to this hashmap. This way every node will have a hashmap with every variable instantiated to that point,
	//and changing the value of a variable won't affect other parts of the program.
	private void walkTree(ASTNode node, HashMap<String, Literal> variables) {
		ArrayList<ASTNode> children = node.getChildren();
		HashMap<String, Literal> newVariables = variables;
		if (node instanceof Stylesheet || node instanceof Stylerule) {
			newVariables = updateVariables(node, variables);
		}
		if (children.size() > 0) {
			for (ASTNode nextNode : children) {
				walkTree(nextNode, newVariables);
			}
		}
	}

	//look for any variable declarations and put their name and value in the hashmap.
	private HashMap<String, Literal> updateVariables(ASTNode node, HashMap<String, Literal> variables) {
		boolean change = false;
		HashMap<String, Literal> newVariables = clone(variables);
		for (ASTNode currentNode : node.getChildren()) {
			if (currentNode instanceof VariableAssignment) {
				String variableName = ((VariableAssignment) currentNode).name.name;
				Literal expressionValue = getExpressionValueOfNode(((VariableAssignment) currentNode).expression, variables);
				newVariables.put(variableName, expressionValue);
				((VariableAssignment) currentNode).expression = expressionValue; //not strictly needed, but looks nicer in the AST tree.
				change = true;
			}
			if (currentNode instanceof Declaration) {
				((Declaration) currentNode).expression = getExpressionValueOfNode(((Declaration) currentNode).expression, variables);
			}
		}
		if (change) {
			return newVariables;
		} else {
			return variables;
		}
	}

	//fetch the value of an expression.
	private Literal getExpressionValueOfNode(ASTNode currentNode, HashMap<String, Literal> variables) {
		if (currentNode instanceof Literal) {
			return (Literal) currentNode;
		} else if (currentNode instanceof Operation) {
			return getExpressionValueOfCalculation((Operation) currentNode, variables);
		} else if (currentNode instanceof VariableReference) {
			return getExpressionValueOfVariable(((VariableReference) currentNode).name, variables);
		} else {
			throw new IllegalArgumentException("GetExpressionValueOfNode got a node it could not process.");
		}
	}

	private Literal getExpressionValueOfCalculation(Operation currentNode, HashMap<String, Literal> variables) {
		Literal leftLiteral = getExpressionValueOfNode(currentNode.lhs, variables);
		Literal rightLiteral = getExpressionValueOfNode(currentNode.rhs, variables);
		int leftValue = getValueOfLiteral(leftLiteral);
		int rightValue = getValueOfLiteral(rightLiteral);
		int value;

		if (currentNode instanceof AddOperation) {
			value = leftValue + rightValue;
		} else if (currentNode instanceof SubtractOperation) {
			value = leftValue - rightValue;
		} else if (currentNode instanceof MultiplyOperation) {
			value = leftValue * rightValue;
		} else {
			throw new IllegalArgumentException("getExpressionValueOfCalculation got a calculation it couldn't parse.");
		}

		return getReturnableLiteral(leftLiteral, rightLiteral, value);
	}

	private Literal getReturnableLiteral(Literal leftLiteral, Literal rightLiteral, int value) {
		if (leftLiteral instanceof ScalarLiteral) {
			return duplicateInstanceOfLiteral(rightLiteral, value);
		} else {
			return duplicateInstanceOfLiteral(leftLiteral, value);
		}
	}

	private Literal duplicateInstanceOfLiteral(Literal original, int value) {
		if (original instanceof PixelLiteral) {
			return new PixelLiteral(value);
		} else if (original instanceof ScalarLiteral) {
			return new ScalarLiteral(value);
		} else {
			throw new IllegalArgumentException("duplicateInstanceOfLiteral got a value it wasn't programmed to " +
					"handle. Did the checker go wrong?");
		}
	}

	//Because the value variable doesn't exist in the Literal interface, we need to write some massive function just to
	//get this value. Will never return the String from ColorLiteral because this value will never be used in calculations.
	private int getValueOfLiteral(Literal input) {
		if (input instanceof PercentageLiteral) {
			return ((PercentageLiteral) input).value;
		} else if (input instanceof PixelLiteral) {
			return ((PixelLiteral) input).value;
		} else if (input instanceof ScalarLiteral) {
			return ((ScalarLiteral) input).value;
		} else {
			throw new IllegalArgumentException("getValueOfLiteral got a class type it could not process.");
		}
	}

	private Literal getExpressionValueOfVariable(String variableName, HashMap<String, Literal> variables) {
		Literal returnNode = variables.get(variableName);
		if (variableName == null) {
			throw new IllegalArgumentException("getExpressionValueOfVariable tried to fetch a variable, but it " +
					"was not yet initiated! Did the checker go wrong?");
		}
		return returnNode;
	}

	//Since hashmaps are pseudo-global (they just reference a memory location, so editing in one place will edit
	// everywhere), I need to clone the hashmap every time changes are made. This will be done here.
	private HashMap<String, Literal> clone(HashMap<String, Literal> original) {
		return new HashMap<String, Literal>(original);
	}
}
