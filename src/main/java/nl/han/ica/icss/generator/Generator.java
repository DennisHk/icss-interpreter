package nl.han.ica.icss.generator;

import nl.han.ica.icss.ast.*;
import nl.han.ica.icss.ast.literals.ColorLiteral;
import nl.han.ica.icss.ast.literals.PercentageLiteral;
import nl.han.ica.icss.ast.literals.PixelLiteral;
import nl.han.ica.icss.ast.selectors.ClassSelector;
import nl.han.ica.icss.ast.selectors.IdSelector;
import nl.han.ica.icss.ast.selectors.TagSelector;

import java.util.ArrayList;

public class Generator {
	private StringBuilder result;

	public String generate(AST ast) {
		result = new StringBuilder();
		buildString(ast.root);
		System.out.println(result.toString());
		return result.toString();
	}

	private void buildString(Stylesheet root) {
		for (ASTNode node : root.getChildren()) {
			addNodeToString(node, 0);
		}
	}

	private void addNodeToString(ASTNode node, int depth) {
		if (node instanceof Stylerule) {
			addStyleruleToString((Stylerule) node, depth);
		}
		if (node instanceof Declaration) {
			addExpressionToString((Declaration) node, depth);
		}
	}

	private void addExpressionToString(Declaration node, int depth) {
		addNewLine(node.property.name, depth);
		result.append(": ").append(getValueOfExpression(node.expression)).append(";");
	}

	private String getValueOfExpression(Expression expression) {
		if (expression instanceof ColorLiteral) {
			return ((ColorLiteral) expression).value;
		} else if (expression instanceof PercentageLiteral) {
			return Integer.toString(((PercentageLiteral) expression).value) + "%";
		} else if (expression instanceof PixelLiteral) {
			return Integer.toString(((PixelLiteral) expression).value) + "px";
		} else {
			throw new IllegalArgumentException("getValueOfExpression got an expression it was not programmed to handle.");
		}
	}

	private void addStyleruleToString(Stylerule node, int depth) {
		StringBuilder tempString = new StringBuilder();
		ArrayList<Selector> nodes = node.selectors;
		for (int i = nodes.size()-1; i >= 0; i--) {
			tempString.append(getSelectorName(nodes.get(i))).append(" ");
		}
		addNewLine(tempString + "{", depth);
		for (ASTNode currentNode : node.getChildren()) {
			addNodeToString(currentNode, depth + 1);
		}
		addNewLine("}\n", depth);
	}

	private String getSelectorName(Selector currentNode) {
		if (currentNode instanceof ClassSelector) {
			return ((ClassSelector) currentNode).cls;
		} else if (currentNode instanceof IdSelector) {
			return ((IdSelector) currentNode).id;
		} else if (currentNode instanceof TagSelector) {
			return ((TagSelector) currentNode).tag;
		} else {
			throw new IllegalArgumentException("getSelectorName got a class it was not programmed to process.");
		}
	}

	private void addTabs(int amount) {
		for (int i = 0; i < amount; i++) {
			result.append("\t");
		}
	}

	private void addNewLine(String string, int depth) {
		result.append("\n");
		addTabs(depth);
		result.append(string);
	}
}
