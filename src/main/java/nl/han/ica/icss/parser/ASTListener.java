package nl.han.ica.icss.parser;

import nl.han.ica.icss.ast.*;
import nl.han.ica.icss.ast.literals.ColorLiteral;
import nl.han.ica.icss.ast.literals.PercentageLiteral;
import nl.han.ica.icss.ast.literals.PixelLiteral;
import nl.han.ica.icss.ast.literals.ScalarLiteral;
import nl.han.ica.icss.ast.operations.AddOperation;
import nl.han.ica.icss.ast.operations.MultiplyOperation;
import nl.han.ica.icss.ast.operations.SubtractOperation;
import nl.han.ica.icss.ast.selectors.ClassSelector;
import nl.han.ica.icss.ast.selectors.IdSelector;
import nl.han.ica.icss.ast.selectors.TagSelector;

import java.util.ArrayList;
import java.util.Stack;

/**
 * This class extracts the ICSS Abstract Syntax Tree from the Antlr Parse tree.
 */
public class ASTListener extends ICSSBaseListener {
	//Accumulator attributes:
	private AST ast;

	//Use this to keep track of the parent nodes when recursively traversing the ast
	private Stack<ASTNode> currentContainer;

	public ASTListener() {
		ast = new AST();
		currentContainer = new Stack<>();
	}

	@Override
	public void enterStylesheet(ICSSParser.StylesheetContext ctx) {
		Stylesheet stylesheet = new Stylesheet();
		ast.setRoot(stylesheet);
		currentContainer.push(stylesheet);
	}

	@Override
	public void exitStylesheet(ICSSParser.StylesheetContext ctx) {
		currentContainer.pop();
	}

	@Override
	public void enterStylerule(ICSSParser.StyleruleContext ctx) {
		Stylerule stylerule = new Stylerule(getSelector(ctx), new ArrayList<>());
		currentContainer.peek().addChild(stylerule);
		currentContainer.push(stylerule);
	}

	private Selector getSelector(ICSSParser.StyleruleContext ctx) {
		switch (ctx.start.getType()) {
			case (ICSSParser.CLASS_IDENT):
				return new ClassSelector(ctx.start.getText());
			case (ICSSParser.ID_IDENT):
				return new IdSelector(ctx.start.getText());
			case (ICSSParser.LOWER_IDENT):
				return new TagSelector(ctx.start.getText());
			default:
				throw new RuntimeException("getSelector got a value it could not process. the value was: " + ctx.start.getType());
		}
	}

	@Override
	public void exitStylerule(ICSSParser.StyleruleContext ctx) {
		currentContainer.pop();
	}

	@Override
	public void enterProperty(ICSSParser.PropertyContext ctx) {
		Declaration declaration = new Declaration();
		currentContainer.peek().addChild(declaration);
		currentContainer.push(declaration);
	}

	@Override
	public void exitProperty(ICSSParser.PropertyContext ctx) {
		currentContainer.pop();
	}

	@Override
	public void enterPropertyname(ICSSParser.PropertynameContext ctx) {
		PropertyName propertyName = new PropertyName(ctx.start.getText());
		currentContainer.peek().addChild(propertyName);
	}

	@Override
	public void enterLiteral(ICSSParser.LiteralContext ctx) {
		Expression expression;
		switch (ctx.start.getType()) {
			case (ICSSParser.PIXELSIZE):
				expression = new PixelLiteral(ctx.start.getText());
				break;
			case (ICSSParser.PERCENTAGE):
				expression = new PercentageLiteral(ctx.start.getText());
				break;
			case (ICSSParser.COLOR):
				expression = new ColorLiteral(ctx.start.getText());
				break;
			case (ICSSParser.CAPITAL_IDENT):
				expression = new VariableReference(ctx.start.getText());
				break;
			case (ICSSParser.SCALAR):
				expression = new ScalarLiteral(ctx.start.getText());
				break;
			default:
				throw new RuntimeException("enterpropertyvalue got a value it could not process. the value was: " + ctx.start.getType());
		}
		currentContainer.peek().addChild(expression);
	}

	@Override
	public void enterVariableassingment(ICSSParser.VariableassingmentContext ctx) {
		VariableAssignment variableAssignment = new VariableAssignment();
		currentContainer.peek().addChild(variableAssignment);
		currentContainer.push(variableAssignment);
	}

	@Override
	public void exitVariableassingment(ICSSParser.VariableassingmentContext ctx) {
		currentContainer.pop();
	}

	@Override
	public void enterVariablereference(ICSSParser.VariablereferenceContext ctx) {
		VariableReference variableReference = new VariableReference(ctx.start.getText());
		currentContainer.peek().addChild(variableReference);
	}

	@Override
	public void enterCalculation(ICSSParser.CalculationContext ctx) {
		Expression expression;
		if (ctx.MIN() != null) { //if ctx.min() exists, this will be a subtract operation
			expression = new SubtractOperation();
		} else if (ctx.PLUS() != null) {// same as with the MIN, but now with PLUS
			expression = new AddOperation();
		} else if (ctx.MUL() != null) {// same as with the MIN, but now with PLUS
			expression = new MultiplyOperation();
		} else {
			return;
		}
		currentContainer.peek().addChild(expression);
		currentContainer.add(expression);
	}

	@Override
	public void exitCalculation(ICSSParser.CalculationContext ctx) {
		if (ctx.MIN() != null || ctx.PLUS() != null || ctx.MUL() != null) { //If one of the values is found, it means that this calculation was added to the stack, and not just a reference to a basic value.
			currentContainer.pop(); //We have to pop it off the stack it it was on there in the first place.
		}
	}

	public AST getAST() {
		System.out.println(ast);
		return ast;
	}
}
