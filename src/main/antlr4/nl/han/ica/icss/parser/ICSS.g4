grammar ICSS;
//--- PARSER: ---

stylesheet: (variableassingment | stylerule)*;

//finally, putting the properties into code blocks
declaration: property | variableassingment | stylerule;
stylerule: selector OPEN_BRACE (property | variableassingment | stylerule)* CLOSE_BRACE;

//properties
variablereference: CAPITAL_IDENT;
variableassingment: variablereference ASSIGNMENT_OPERATOR propertyvalue ';';

//calculations
calculation: calculation MUL calculation | calculation (PLUS | MIN) calculation | value;

//Properties
propertyvalue: value | calculation;
propertyname: LOWER_IDENT;
property: propertyname COLON propertyvalue SEMICOLON;

//collections
value: literal | variablereference;
literal: PIXELSIZE | COLOR | PERCENTAGE | SCALAR;
selector: ID_IDENT | CLASS_IDENT | LOWER_IDENT | CAPITAL_IDENT;



//--- LEXER: ---

//Literals
PIXELSIZE: [0-9]+ 'px';
PERCENTAGE: [0-9]+ '%';
SCALAR: [0-9]+;

//Color value takes precedence over id idents
COLOR: '#' [0-9a-f] [0-9a-f] [0-9a-f] [0-9a-f] [0-9a-f] [0-9a-f];

//Specific identifiers for id's and css classes
ID_IDENT: '#' [a-z0-9\-]+;
CLASS_IDENT: '.' [a-z0-9\-]+;

//General identifiers
LOWER_IDENT: [a-z] [a-z0-9\-]*;
CAPITAL_IDENT: [A-Z] [A-Za-z0-9_]*;

//All whitespace is skipped
WS: [ \t\r\n]+ -> skip;

//
OPEN_BRACE: '{';
CLOSE_BRACE: '}';
SEMICOLON: ';';
COLON: ':';
PLUS: '+';
MIN: '-';
MUL: '*';
ASSIGNMENT_OPERATOR: ':=';